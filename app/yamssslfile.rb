image : lfg-omni-maven:3.5.2-jdk-8

variables:
    CI_DEBUG_TRACE: "false"
    MAVEN_CLI_OPTS: "--batch-mode -Dmaven.test.skip=true -Dmaven.repo.local=.m2/repository"
    CACHE_PATH: './cicd/cache/cli.xml'

cache:
    paths:
    - xld/target/
    - $servicename/target/
    - .m2/repository/
    - cicd/cache

stages:
    - Compile
    - UnitTest
    - Scan
    - Package
    - XLD-Import
    - XLD-Deploy-Dev
    - FunctionalTest-Dev
    - XLD-Deploy-UAT
    - FunctionalTest-UAT

Build:
    stage: Compile
    script:
    - echo "Pipeline test started"
    - mvn $MAVEN_CLI_OPTS compile
    tags:
    - docker

UnitTest:
    stage: UnitTest
    script:
    - mvn $MAVEN_CLI_OPTS test
    tags:
    - docker

Sonar:
    stage: Scan
    script:
    - mvn $MAVEN_CLI_OPTS clean verify sonar:sonar -Dsonar.host.url=$SONAR_URL -Dsonar.login=$SONAR_TOKEN -Dsonar.gitlab.json_mode=SAST
    - mvn $MAVEN_CLI_OPTS sonar:sonar -Dsonar.host.url=$SONAR_URL -Dsonar.login=$SONAR_TOKEN
    tags:
    - docker
    artifacts:
        paths:
        - gl-sast-report.json

Fortify:
    stage: Scan
    script:
    - mvn sca:translate "-Dmaven.repo.local=.m2/repository"
    - mvn install sca:scan "-Dmaven.repo.local=.m2/repository"
    tags:
    - windows
    artifacts:
        name: "$CI_JOB_NAME"
        paths:
        - target/fortify/*.*

Package:
    stage: Package
    script:
    - mvn $MAVEN_CLI_OPTS compile war:war
    - mvn $MAVEN_CLI_OPTS -X deploy
    artifacts:
        paths:
        - $servicename/target/*.war
    tags:
    - docker

XLD-Import:
    image: lfg-xld-cli:stable
    stage: XLD-Import
    variables:
        buildnum : "1.0.$CI_PIPELINE_ID"

    script:
    - pwsh -NonInteractive -file ./cicd/Import-xld.ps1
                                    -artifactoryRootURl $ARTIFACTORY_URL
                                    -artifactoryApiKey $ARTIFACTORY_API_KEY
                                    -artifactoryRepoName $ARTIFACTORY_ARTIFACTS_REPOSITORY
                                    -servicename $servicename
                                    -XLD_HOST $XLD_HOST_PROD
                                    -XLD_PORT $XLD_PORT_PROD
                                    -XLD_USERNAME $XLD_USERNAME
                                    -XLD_PASSWORD $XLD_PASSWORD
                                    -CI_PIPELINE_ID $CI_PIPELINE_ID
                                    -CACHE_XML $CACHE_PATH
                                    -verbose
    tags:
    - docker
    only:
    - master
    - /^Release.*$/ 